import pandas as pd 
from train_model_LDA import LDA_model
from train_model_LSA import LSA_model
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
import ast

class chat(object): 
    def __init__(self, model: str = "LDA"): 
        import fasttext
        import pickle 
        self.model_vec = fasttext.load_model("cc.en.300.bin")
        self.table_vectors = pd.read_csv(f"datasets/table_vectors_{self.model}.csv")
        
        if self.model == "LDA": 
            modelo = "models/lda_model.pkl"
        if self.model == "LSA": 
            modelo = "models/lsa_model.pkl"

        with open(modelo, "rb") as file: 
            self.model = pickle.load(file)
        print("""
            Chat Listo. 
            Solo permite comunicación en Inglés
        """)
        try: 
            print(f"Test data shape: {self.model.test_data.shape}")
            print(f"Train data shape: {self.model.train_data.shape}")
            print(self.model.train_data.shape)
        except Exception as e: 
            print(f"Can not find Test and train data due to: {e}")
        print("Hi! How i can Help  you?")
        self.question = input()
        while self.question != "Adios": 
            self.__get_question_related(self.question)
            self.__get_responses_related()
            self.__get_main_table()
            self.__get_answer()
            self.question = input()

    def __get_question_related(self,question):
        self.question_vec = self.model_vec.get_sentence_vector(question)
        self.question_top = self.model.predict([question])["Predict Topic"]["index"]
        return self
        
    def __get_responses_related(self): 
        self.indexes = [i for i, x in enumerate(self.table_vectors["topics"]) if x ==self.question_top]
        # utilizar numpy para las comparaciones. 
        vectores = self.table_vectors["vectores"]
        self.vectors = vectores[self.indexes]

    def __get_main_table(self): 
        """
        La tabla tarda demasiado en ser construida. Por que calcula la distancia con cada una de las posibles respuestas dentro del topico. 
        """
        # Cambiar la logica para usar numpy en lugar de pandas. 
        lista = []
        # guardar solo si el nuevo valor es mayor que el mínimo del array¡'
        for i in self.vectors:
            i = np.asarray(ast.literal_eval(i))
            lista.append(cosine_similarity(self.question_vec.reshape(1, -1), i.reshape(1, -1))[0][0])
        tabla = pd.concat([pd.DataFrame({"lista":lista}), pd.DataFrame({"indices": self.indexes})], axis = 1)
        tabla.sort_values("lista", ascending = False, inplace = True)
        self.tabla = tabla.head(10)

    def __get_answer(self): 
        self.responses = self.table_vectors["responses"]
        respuesta  = self.tabla["indices"].iloc[0]
        respuesta = self.responses[respuesta]
        print(respuesta)


            



if __name__ == "__main__": 
    chat(model = "LSA")