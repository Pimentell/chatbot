1. Experimentación sobre los diferentes modelos
    - models
        - Baseline random
        - Baseline random by topic
    - F1 score
    - Recall
    - Accuracy
    - Brier Score
2. Documento sobre metodología y resultados
    - Evaluation Metodology
    - Metodology
    - Discusiones
    - Conclusiones

"""
    Every Response over 0.8 it is a good response: 1
    Other: 0 
    F_score
    Precision
    Accuracy
    Change SVM over 
"""
    resultados = []
    for vector_answer, question in zip(data["vectores"][:10], data["questios"][:10]): 
        prediction = model.predict([question])
        prediction_topic = prediction["Predict Topic"]["index"]
        indexes = [i for i, x in enumerate(data["topics"]) if x ==prediction_topic]
        vectores = data["vectores"]
        vectors = vectores[indexes]
        print(f"Questions Related: {vectors.shape}")
        results = []
        for i in vectors[:100]:   
            try:   
                i = np.asarray(ast.literal_eval(i))
                vector_answer = np.asarray(ast.literal_eval(vector_answer))
                similarity = cosine_similarity(vector_answer.reshape(1, -1), i.reshape(1, -1))[0][0]
                results.append(similarity)
                tabla = pd.concat([pd.DataFrame({"lista":results}), pd.DataFrame({"indices": indexes})], axis = 1)
                tabla.sort_values("lista", ascending = False, inplace = True)
                responses = data["vectores"]
            except Exception as e: 
                pass
            respuesta = tabla["indices"].iloc[0]
            respuesta = responses[respuesta]
            respuesta = np.asarray(ast.literal_eval(respuesta))
            resultados.append(cosine_similarity(vector_answer.reshape(1, -1), respuesta.reshape(1, -1))[0][0])
            resultados = resultados
    resultados = np.asarray(resultados)
    print(resultados.mean())