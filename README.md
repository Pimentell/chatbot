# Chatbot based on Twitter

## Objetivo

Crear un servicio de chatbot que permita resolver consultas de clientes a través de un chatbot. Se utilizará información de la base de datos de Kaggle:

https://www.kaggle.com/thoughtvector/customer-support-on-twitter

Steps: 

Consideraciones:  Revisar el comportamiento dependiendo del tipo de tratamiento a los emojis

Topico y categoría por pregunta: 

1. Kmeans para clustering de vectores de palabras mucho mas cercanas para labeling
2. Entrenar el dataset para identificar sólo el topico o la clase de las preguntas
3. Encontrar similitud de lo que dijo el usuario con las preguntas que tenemos registradas (cosine similarity)

tag = "class"
pattern = "question"
response = "answer"

Tipos de Categorias: 

    training_sentences = [
    "What would you like to have for dinner?",
    "What do you want to eat tonight?",
    "I don't know what to cook tonight.",
    "Do you have any cravings?",
    "Can I get you something to eat?", 
    "What time will you be home?",
    "How much longer will you be?",
    "When can we expect you to get here?",
    "What's taking you so long?",
    "At what hour will you be here?"
    ]
    
    training_intents = [
        "dinner_preference",
        "dinner_preference",
        "dinner_preference",
        "dinner_preference",
        "dinner_preference",
        "arrival_time",
        "arrival_time",
        "arrival_time",
        "arrival_time",
        "arrival_time"   
    ]

Clean Data process

El médoto utilizado para limpiar la base de datos de salida se puuede encontrar en la siguiente liga: 
    - https://www.kaggle.com/clmentbisaillon/twitter-customer-support-data-cleaning

Database Explanation: 

tweet_id: identificador del tweet 
author_id: usuario o personal de servicio al cliente. En caso de usuario tiene un valor int
inbound: Boleean 