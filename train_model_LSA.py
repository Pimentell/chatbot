import nltk
import ssl
import pandas as pd 
import numpy as np

import warnings 
warnings.simplefilter("ignore", DeprecationWarning)

# Verificacion del certificado ssl para descarga a traves de nltk.download
try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    pass
else:
    ssl._create_default_https_context = _create_unverified_https_context
    

from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import TruncatedSVD
from utils.utils import Cleaner
import matplotlib.pyplot as plt
import pickle

class LSA_model(): 
    def __init__(self, data = None):  
        """
        Print ("Hola")
        """
        if data == None: 
            data = pd.read_csv("data/final_data.csv")[["question", "response"]]
        assert len(data) > 0 , "Can't find data"
        data.dropna(subset = ["question"], inplace = True) 
        data.drop_duplicates(subset = ['ID'], keep = 'first', inplace = True) 

        splitter = int(0.8 * (len(data)))
        self.train_data = data[:splitter]
        self.test_data = data[splitter:]
        self.vectorizer = CountVectorizer(stop_words = 'english')

    def topics(self, model, n_top_words): 
        words = self.vectorizer.get_feature_names() # El objeto words es el corpus de todos los textos. 
        dic = {}
        for topic_idx, topic in enumerate(model.components_):
            dic.update({topic_idx:" ".join([words[i] for i in topic.argsort()[:-n_top_words - 1:-1]]) })
        return dic
    
    def transform(self, data = None, number_words: int = 5):
        """
            Get document_term_matrix
        """
        responses = self.train_data.response
        questions = self.train_data.question
        self.number_words = number_words
        X = self.vectorizer.fit_transform(questions)
        print("Final DataFrame Shape: ", X.shape)
        self.X = X
        return self

    def fit(self, number_of_topics: int = 5):
        lsa_model = TruncatedSVD(n_components=number_of_topics, algorithm='randomized', n_iter=400, random_state=122)
        lsa_model.fit(self.X)
        self.model = lsa_model
        self.dic_topics =  self.topics(self.model, self.number_words)
        return self
    
    def predict(self, text): 
        X_test = self.vectorizer.transform(text)
        prueba = self.model.transform(X_test)
        salida  = prueba.argmax(axis = 1)
        dic = {"Predict Topic": {"index":salida[0], "text": self.dic_topics[salida[0]]}}
        return dic
        
if __name__ == "__main__": 
    model = LSA_model() 
    model.transform()
    model.fit(number_of_topics= 5)
    print(model.dic_topics)
    print(model.predict(["I i have a problem whit my batery"]))

    with open("models/lsa_model.pkl", "wb") as file: 
        pickle.dump(model, file)
