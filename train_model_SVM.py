import nltk
import ssl

import warnings 
warnings.simplefilter("ignore", DeprecationWarning)

# Verificacion del certificado ssl para descarga a traves de nltk.download
try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    pass
else:
    ssl._create_default_https_context = _create_unverified_https_context
        
import string
from gensim import corpora
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords, wordnet
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation as LDA
from sklearn import svm

import pandas as pd 
import numpy as np
from utils.utils import Cleaner
import pickle


class SVM_model(): 
    def __init__(self, lista): 
        self.count_vectorizer = CountVectorizer(stop_words = 'english')
        self.lista = lista
        
    def transform(self, number_topics = 5, number_words = 5):
        self.count_data = self.count_vectorizer.fit_transform(self.lista)
        self.number_topics = number_topics
        self.number_words = number_words
        return self

    def topics(self, model,count_vectorizer, n_top_words): 
        words = self.count_vectorizer.get_feature_names() # El objeto words es el corpus de todos los textos. 
        dic = {}
        for topic_idx, topic in enumerate(model.components_):
            dic.update({topic_idx:" ".join([words[i] for i in topic.argsort()[:-n_top_words - 1:-1]]) })
        return dic


if __name__ == "__main__": 
    data = pd.read_csv("data/final_data.csv")
    data = data[["ID", "question", "response", "empresa"]]
    data.dropna(subset = ["question"], inplace=True)
    data.drop_duplicates(subset = ['ID'], keep = 'first', inplace = True) 
    
    modelo = SVM_model(lista =data["question"])
    #modelo.fit()