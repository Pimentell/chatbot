import pandas as pd
import numpy as np
import re 
import string
import nltk

from langdetect import detect
from nltk.stem.wordnet import WordNetLemmatizer
from gensim import corpora
from nltk.corpus import stopwords, wordnet

tagging_regex = re.compile(r"@\S*")
url_pattern = re.compile(r'https?://\S+|www\.\S+')
signature_pattern = re.compile(r"-\S*")
weird_thing_pattern = re.compile(r"\^\S*")
new_line_pattern = re.compile(r"\n+\S*")
   

class extract(): 
    __name__ = "Extract Data"
    PATH = "data/twcs.csv"
    def __init__(self): 
        pass
    @classmethod
    def get_data(cls): 
        data = pd.read_csv(cls.PATH,
        usecols = [
            'author_id', 
            'inbound', 
            'text', 
            'in_response_to_tweet_id'], 
        dtype = {
            'author_id': str,
            'inbound': np.bool_,
            'text': str,
            'in_response_to_tweet_id': pd.Int64Dtype()
        })
        conversations = []
        last_end_of_conversation = 0
        start_discussion_indices = data.index[data["in_response_to_tweet_id"].isna()].tolist()
        for index in start_discussion_indices: 
            conversations.append(data[last_end_of_conversation : index + 1].drop('in_response_to_tweet_id', axis=1)) 
            last_end_of_conversation = index + 1
        for conversation in conversations:
            lastInbound = None
            rows_to_remove = []
            messages_to_append = {}
            for index, row in conversation.iterrows():
                inbound = row['inbound']
                message = row['text']
                # Dos mensajes consecutivos de la misma persona 
                if lastInbound is not None and (lastInbound == inbound):
                    # Elimina el Row
                    rows_to_remove.append(index)
                    if not index - 1 in messages_to_append.keys():
                        messages_to_append[index - 1] = []
                    messages_to_append[index - 1].append(message)
                    pass
                lastInbound = inbound
            for index, message_to_add in messages_to_append.items():
                conversation.at[index, 'text'] = conversation.loc[index]['text'] + '. ' + message_to_add[0]
            conversation.drop(rows_to_remove, axis=0, inplace=True)
        return conversation

class clean(): 
    __name__ = "Class for cleaning the data"
    def __init__(self): 
        from clean_data_utils import chat_words, contractions
        self.chat_words = chat_words
        self.contractions = contractions
        self.PUNCT_TO_REMOVE = string.punctuation

    # En referencia a  : https://stackoverflow.com/a/49986645/3971619
    def remove_emoji(self, inputString):
        return inputString.encode('ascii', 'ignore').decode('ascii')

    # Thanks to user sudalairajkumar
    def remove_url(self, string):
        return url_pattern.sub(r'', string)

    def remove_chat_words_and_contractions(self,string):
        new_text = []
        for word in string.split(' '):
            if word.upper() in self.chat_words.keys():
                new_text += self.chat_words[word.upper()].lower().split(' ')
            if word.lower() in self.contractions.keys():
                new_text += self.contractions[word.lower()].split(' ')
            else:
                new_text.append(word)
            
        return ' '.join(new_text)

    def remove_signature(self, text):
        return signature_pattern.sub(r'', text)


        # Thanks to user sudalairajkumar
    def remove_punctuation(self, text):
        """custom function to remove the punctuation"""
        return text.translate(str.maketrans('', '', self.PUNCT_TO_REMOVE))

    def clean_message(self, message):
        # Remove user taggings
        message = re.sub(tagging_regex, '', message) # Replace by you. Good idea?

        # Remove the emojis
        message = self.remove_emoji(message)

        # Remove urls
        message = self.remove_url(message)

        # Remove signatures
        message = self.remove_signature(message)

        # Remove the chat words and contractions
        message = self.remove_chat_words_and_contractions(message)

        # Remove weird things
        message = weird_thing_pattern.sub(r'', message)

        # Change new line to dot
        message = new_line_pattern.sub(r'.', message)

        # Remove punctuation
        message = self.remove_punctuation(message)

        # Remove start and end whitespace
        message = message.strip()

        # Make multiple spaces become a single space
        message = ' '.join(message.split())

        # Lower case the message
        message = message.lower()

        # If not in english, return empty string
    #     if message and len(message) > 15:
    #         if detect(message) != 'en':
    #             return ""
        return message


class Cleaner(object): 
    def __init__(self): 
        """
        Own class for cleaning text
        """
        self.punctuation = set(string.punctuation)

        self.stoplist = set(stopwords.words('english'))
        
        self.dictionary = corpora.Dictionary()
        self.lemma = WordNetLemmatizer()
        
    def remove_punctuation(self, text): 
        return ''.join([char for char in text if char not in self.punctuation])

    def remove_numbers(self, text):
        return ''.join([char for char in text if not char.isdigit()])
        
    def remove_stopwords(self, text):
        return ' '.join([word for word in text.split() if word not in self.stoplist])
        
    def remove_single_chars(self, text):
        return ' '.join([word for word in text.split() if len(word) > 1])
        
    def lemmatize(self, text):
        return ' '.join([self.lemma.lemmatize(word) for word in text.split()])
        
    def clean_text(self, text):
        text = text.replace('\n', '')
        text = self.remove_punctuation(text)
        text = self.remove_numbers(text)
        text = self.remove_stopwords(text)
        text = self.remove_single_chars(text)
        text = self.lemmatize(text)
        return text
if __name__ == "__main__": 
    # data = extract.get_data()
    # print(data.head())
    data = pd.read_csv("data/apple_data.csv")
    data["question"] = data["question"].apply(lambda x: clean().clean_message(x))
    data["question"] = data["question"].apply(lambda x: Cleaner().clean_text(x))
    data.to_csv("data/final_data.csv")