from train_model_LDA import LDA_model
from train_model_LSA import LSA_model
import pickle
import fasttext
import pandas as pd
import numpy as np
import ast
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score # Average
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
import fasttext


def test(model, data, sample = 1000): 
    print("Loading Model")
    with open(model, "rb") as file: 
        model = pickle.load(file)
    model_vec = fasttext.load_model("cc.en.300.bin")
    print("Model Loaded")
    print(f"Test data has shape:{model.test_data.shape}")

    table_vectors = pd.read_csv(f"datasets/table_vectors_LDA.csv")

    test_data = model.test_data.sample(n = sample, random_state = 1)

    test_data_questions = test_data["question"]
    test_data_responses = test_data["response"]
    # Similarity of the cuestion
    results = []
    contador = 1
    for test_question, test_response in zip(test_data_questions, test_data_responses):
        print(f"Avance: {contador}")
        test_question_vec = model_vec.get_sentence_vector(test_question)
        test_question_topic = model.predict([test_question])["Predict Topic"]["index"]
        indexes = [i for i, x in enumerate(table_vectors["topics"]) if x ==test_question_topic]
        vectores = table_vectors["vectores"]
        vectors = vectores[indexes]
        best_question_sup = [0]
        best_question = {} # Dict of best questions
        for vector_index in vectors.index: 
            vector = np.asarray(ast.literal_eval(vectors[vector_index]))
            similarity = cosine_similarity(test_question_vec.reshape(1, -1), vector.reshape(1, -1))[0][0]
            if similarity > max(best_question_sup) and int(similarity) != 1: # Se elimina el valor que es igual con uno. 
                best_question["result"] = {"vector": vector_index, "similarity": similarity}
                best_question_sup.append(similarity)
        test_response_question = table_vectors[table_vectors.index == best_question["result"]["vector"]]["responses"].iloc[0]
        test_response_vec_pred = model_vec.get_sentence_vector(test_response_question)
        test_response_vec_real = model_vec.get_sentence_vector(test_response)
        similarity = cosine_similarity(test_response_vec_pred.reshape(1, -1), test_response_vec_real.reshape(1, -1))[0][0]
        if similarity >= 0.90: results.append(1)
        if similarity < 0.90: results.append(0)
        contador += 1
    return results

def calculate_metrics(sample, data): 
    perfect_comparison = [1] * sample
    accuracy = accuracy_score(perfect_comparison, data)
    precision = precision_score(perfect_comparison,data)
    recall = recall_score(perfect_comparison, data)
    f1score = f1_score(perfect_comparison, data)
    print(f"Accuracy: {accuracy}")
    print(f"Precision: {precision}")
    print(f"Recall: {recall}")
    print(f"F1_score: {f1score}")
    return {
        "accuracy": accuracy, 
        "precision": precision, 
        "recall": recall, 
        "f1_score":f1score
    }



if __name__ == "__main__": 
    #modelo1 = "models/lda_model.pkl"
    modelo2 = "models/lsa_model.pkl"

    #data1 = pd.read_csv("datasets/table_vectors_LDA.csv")
    data2 = pd.read_csv("datasets/data_topics_LSA.csv")

    total_data = pd.read_csv("data/final_data.csv")

    #Extract values for predictions
    sample = 100
    results = test(modelo2, data2, sample = sample)
    metrics = calculate_metrics(data = results, sample = sample)

