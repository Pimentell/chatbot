import fasttext
import numpy as np 
import pickle
import pandas as pd 
from train_model_LDA import LDA_model
from train_model_LSA import LSA_model

class predict_topics(object): 
    def __init__(self, model_name:str = "LDA"): 
        """
        Predice los topicos dentro de los datos originales. 
        Valid values for model: 
            - LDA
            - LSA 
        """
        if model_name == "LDA": 
            with open ("models/lda_model.pkl", "rb") as file: 
                self.model = pickle.load(file)
        if model_name == "LSA": 
            with open("models/lsa_model.pkl", "rb") as file: 
                self.model = pickle.load(file)

        data =pd.read_csv("data/final_data.csv")
        data.dropna(subset = ["question"], inplace = True)
        data.drop_duplicates(subset = ['ID'], keep = 'first', inplace = True) 
        data.drop(columns = ["Unnamed: 0", "Unnamed: 0.1"], inplace = True)
        data["topic"]  = data["question"].apply(lambda x: self.model.predict([x])["Predict Topic"]["index"])
        self.data = data
        self.model_name = model_name
        # Guarda los datos de los topicos y las preguntas y respuestas relacionadas para su posterior uso
        self.data.to_csv(f"datasets/data_topics_{self.model_name}.csv")

class get_vectors(object): 
    def __init__(self, model:str = "LDA"): 
        """
        Crea los vectores de palabras para todas las preguntas dentro del dataset original. 
        """
        import fasttext
        self.model_vec = fasttext.load_model("cc.en.300.bin")
        if model == "LDA": 
            self.data = pd.read_csv("datasets/data_topics_LDA.csv")
        if model == "LSA": 
            self.data = pd.read_csv("datasets/data_topics_LSA.csv")
        self.model = model
        self.gen_vectors()

    def gen_vectors(self): 
        """
        """
        # modificar vectores por numpy  
        vectores = []
        ids = []
        questions = []
        responses = []
        topics = []
        for text, id, response, topic in zip(self.data["question"], self.data["ID"], self.data["response"], self.data["topic"]): 
            vectores.append((np.asarray(self.model_vec.get_sentence_vector(text))).tolist())
            # Verificar si es posible guardar  en numpy
            ids.append(id)
            questions.append(text)
            responses.append(response)
            topics.append(topic)
        print("creación de tabla")
        table = pd.concat([
            pd.DataFrame({"vectores": vectores}), 
            pd.DataFrame({"ids": ids}), 
            pd.DataFrame({"questios": questions}), 
            pd.DataFrame({"topics": topics}), 
            pd.DataFrame({"responses": responses})
        ], axis = 1) 
        self.table = table
        print("Salvado de tabla")
        self.table.to_csv(f"datasets/table_vectors_{self.model}.csv")
        return self

if __name__ == "__main__": 
    print("Comienza Prediccion de topicos")
    predict_topics(model_name = "LDA")
    print("LDA Finished")
    predict_topics(model_name = "LSA")
    print("LSA Finished")
    print("Termina Prediccion de Topicos")
    print()
    print("Comienza Obtención de vectores")
    get_vectors(model = "LDA")
    print("LDA Finished")
    get_vectors(model = "LSA")
    print("LSA Finished")
    print("Termina Obtención de Vectores")

